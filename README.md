# Scrapping

Script de scrapping (Spacefoot.com)

## Installation

### Téléchargement

Commencez par cloner le projet via la commande suivante :

`git clone https://gitlab.com/HARM-and/scrapping.git`

### Mise en place

Une fois le code dans votre dossier, commencez par créer la base de données qui recueillera ce que vous allez récupérez via le script.

Pour cela, vous pouvez utiliser le fichier [bd_spacefoot.sql](https://gitlab.com/HARM-and/scrapping/-/blob/main/bd_spacefoot.sql) fourni.

Vous devriez vous retrouvez avec ceci :

![image](https://media.discordapp.net/attachments/935933560525819964/935934258659340368/unknown.png "Image")

La table "offre" :

![image](https://media.discordapp.net/attachments/935933560525819964/935934326644830208/unknown.png "Image")

Une fois la table en place, vous pouvez modifier le [fichier de config](https://gitlab.com/HARM-and/scrapping/-/blob/main/config.json) avec vos informations personnelles.

![img de config](https://media.discordapp.net/attachments/935933560525819964/935933587260330094/config.PNG "Fichier de config")

## Utilisation

Ouvrez votre invit de commande favori et déplacez vous jusqu'au répértoire du projet.

![image](https://media.discordapp.net/attachments/935933560525819964/935935160698961970/unknown.png "Image")

Une fois dans ce dernier, exécutez le script php avec la commande suivante :

`php Spacefoot.php`

Si tout se passe correctement, votre base de données doit s'être remplie avec les infos sur toutes les offres d'emplois proposées par Spacefoot.

![image](https://media.discordapp.net/attachments/935933560525819964/935935333382635540/unknown.png?width=1434&height=612 "Image")
