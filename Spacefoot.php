<?php

    require __DIR__ . "/vendor/autoload.php";
    use Goutte\Client;
    
    //Classe gérant la connexion MySQL
    class Data
    {
        function connexionPDO()
        {

        	//Récupération des infos du fichier de config
            $data = file_get_contents('.\config.json', FILE_USE_INCLUDE_PATH);
            $config = json_decode($data); 
            $login = $config[0]->login;
            $mdp = $config[0]->mdp;
            $bd = $config[0]->bd;
            $serveur = $config[0]->serveur;

            try
            {
                $conn = new \PDO("mysql:host=$serveur;dbname=$bd", $login, $mdp, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $conn;
            }
            catch (PDOException $e)
            {
                print "Erreur de connexion PDO ";
                die();
            }
        }
    }

    //Récupération du DOM de la page principale
    $client = new Client();
    $crawler = $client->request('GET', 'https://spacefoot.com/jobs');
    $out = fopen('php://output', 'w');
    $crawler->filter('div.container > div.box')->each( function ($node) use ($out) {

    	//Récupération des informations disponible sur la page principale
        $link = $node->filter('h2 > a')->attr('href');
        $titre = $node->filter('h2 > a')->text();
        $titreClear = str_replace(array("/","`","'",","), array("","",""," "), strtolower($titre));
    	$type = $node->filter('p span:nth-child(1)')->text();
        $team = $node->filter('p span:nth-child(2)')->text();
        $localisations = $node->filter('p span:nth-child(n+3)')->each( function ($node) use ($out) {
            $data = "";
            $data .= $node->text();
            return $data;
        });
        $nbLocalisation = count($localisations);

        //Récupération du DOM de la page liée à l'offre
        $client2 = new Client();
        $crawler2 = $client2->request('GET', "https://spacefoot.com$link");

        //Récupération des informations disponible sur la page de l'offre
        $date = $crawler2->filter('div.column > div.box.content > dl > dd:nth-of-type('.(3+$nbLocalisation).')')->text();
        $contenu = str_replace(array("'"), array(" "), $crawler2->filter('div.column.is-three-quarters > div.content')->text());

        //Uniformisation des localisations
        $localList = "";
        foreach ($localisations as $localisation)
        {
            $localList .= ", ".$localisation;
        }
        $localList = substr($localList, 1);
        $localList = str_replace(array("3 SITES : ","-"), array(""," "),$localList);

        //Instanciation d'une connexion à la base de données
        $BDD = new Data;
        try
        {
            // Connexion à la bdd
            $cnx = $BDD->connexionPDO();
            // Préparation de la requête
            $req = $cnx->prepare("insert into `offre` (`titre`, `type`, `team`, `localisation`, `date`, `contenu`) VALUES ('".$titre."', '".$type."', '".$team."', '".$localList."', '".$date."', '".$contenu."')");
            //Exécution de la requête
            $req->execute();
        }
        // En cas d'erreur
        catch (PDOException $e)
        {
            print "Erreur !: " . $e->getMessage();
            die();
        }

    });





